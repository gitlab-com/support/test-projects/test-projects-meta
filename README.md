# Test Projects Meta

This tracker is used to start general conversations around test projects for support team.

Project under [Test Projects](https://gitlab.com/gitlab-com/support/test-projects) can be copied to be use as quick start project/templates for testing features. 

## Getting started

If you are interesting in adding some projects to the [Test Projects](https://gitlab.com/gitlab-com/support/test-projects) please ask one of the group owners to add you as an owner. 
